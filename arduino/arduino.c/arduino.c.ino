
#include <DHT.h>
#include <iarduino_Position_BMX055.h>

#define DHTPIN 2
DHT dht(DHTPIN, DHT22);

iarduino_Position_BMX055 sensorA(BMA); 
iarduino_Position_BMX055 sensorG(BMG); 
iarduino_Position_BMX055 sensorM(BMM); 

#define ANALOG_PIN A0
void setup() 
{
        Serial.begin(9600);     // opens serial port, sets data rate to 9600 bps
        dht.begin();
        sensorA.begin();
        sensorG.begin();
        sensorM.begin();
}

// Is responsible for output for each sensor.
#define BUTTON_MASK         (1 << 0)
#define DTH_MASK            (1 << 1)
#define BMX_ACC_MASK        (1 << 2)
#define BMX_GYRO_MASK       (1 << 3)
#define BMX_MAGN_MASK       (1 << 4)
#define ANALOG_MASK         (1 << 5)


#define HEADER_VALUE   0x3A

#define BUTTON_TYPE    0x00
#define DHT22_TYPE     0x01
#define BMX_ACC_TYPE   0x02
#define BMX_GYRO_TYPE  0x03
#define BMX_MAGN_TYPE  0x04
#define ANALOG_TYPE    0x05


static int BUTTON_SWITCH    = 0;
static int DTH_SWITCH       = 0;
static int BMX_ACC_SWITCH   = 0;
static int BMX_GYRO_SWITCH  = 0;
static int BMX_MAGN_SWITCH  = 0;
static int ANALOG_SWITCH    = 0;

void serve_serial_input()
{
       static int cnt = 0;
       int incomingByte = 0;   // for incoming serial data
       while (Serial.available() > 0) 
       {
                // read the incoming byte:
                incomingByte = Serial.read();
                switch (cnt) {
                   #define HEADER   0
                   #define TYPE     1
                   #define DATA(x)  (x+2)
                   case HEADER:
                        if(incomingByte == HEADER_VALUE)
                             cnt++; break;
                   case TYPE: 
                        cnt++; break;
                   case DATA(0):
                        BUTTON_SWITCH     = (incomingByte & BUTTON_MASK); 
                        DTH_SWITCH        = (incomingByte & DTH_MASK);
                        BMX_ACC_SWITCH    = (incomingByte & BMX_ACC_MASK);
                        BMX_GYRO_SWITCH   = (incomingByte & BMX_GYRO_MASK);
                        BMX_MAGN_SWITCH   = (incomingByte & BMX_MAGN_MASK);
                        ANALOG_SWITCH     = (incomingByte & ANALOG_MASK);
                        cnt++; break;
                   case DATA(1): 
                        cnt++; break;
                   case DATA(2): 
                        cnt++; break;
                   case DATA(3):
                        cnt++; break;                       
                }
       }
}

void send_msg(int type, void *data, int datalen)
{
    #define SEVICE_ADDITIONAL_DATA_LEN  1
    char * d = (char *)data;
    Serial.write(HEADER_VALUE);
    Serial.write(SEVICE_ADDITIONAL_DATA_LEN + datalen);
    Serial.write(type);
    for(int i = 0; i < datalen; i++) Serial.write(d[i]);
}

void serve_dht()
{
  if(DTH_SWITCH)
  {
    int h = (char)dht.readHumidity();
    int t = (char)dht.readTemperature();
    
    char data[2] = {h, t};    
    send_msg(DHT22_TYPE, data, sizeof(data));
  }
}

void serve_button()
{
       #define PRESSED     0x01
       if(BUTTON_SWITCH)
       {
            char data[1] = {PRESSED};    
            send_msg(BUTTON_TYPE, data, sizeof(data));
       }
}

void serve_analog()
{
       if(ANALOG_SWITCH)
       {
            int data[1];
            data[0] = analogRead(ANALOG_PIN);
            send_msg(ANALOG_TYPE, data, sizeof(data));
       }
}
void serve_BMX()
{
      if(BMX_ACC_SWITCH)
      {
          sensorA.read();
          float data[3] = { sensorA.axisX, sensorA.axisY, sensorA.axisZ };
          send_msg(BMX_ACC_TYPE, data, sizeof(data));
      }
      if(BMX_GYRO_SWITCH)
      {
          sensorG.read();
          float data[3] = { sensorG.axisX, sensorG.axisY, sensorG.axisZ };
          send_msg(BMX_GYRO_TYPE, data, sizeof(data));
      }
      if(BMX_MAGN_SWITCH)
      {
          sensorM.read();
          float data[3] = { sensorM.axisX, sensorM.axisY, sensorM.axisZ };
          send_msg(BMX_MAGN_TYPE, data, sizeof(data));
      }
}

void loop() 
{
       // Polling incoming command. 
       serve_serial_input();
       
       // Transmit data from button 
       serve_button();
       
       // Transmit data from dth sensor 
       serve_dht();
      
       // Transmit data from BMX sensor 
       serve_BMX();
       
       // Transmit data from analog sensor 
       serve_analog();
       
}
 
