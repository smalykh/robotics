#ifndef __COMMAND_H___
#define __COMMAND_H___
#include <stdint.h>

#define MAX_COMMAND_LEN
#define COMMAND_PREFIX		0x3a

// Types of incoming commands
#define BUTTON_SENSOR_DATA	0x00
#define DHT22_SENSOR_DATA	0x01
#define BMX_ACC_SENSOR_DATA   	0x02
#define BMX_GYRO_SENSOR_DATA  	0x03
#define BMX_MAGN_SENSOR_DATA  	0x04
#define ANALOG_SENSOR_DATA  	0x05

// Types of outcoming commands
#define BUTTON_SWITCH_MASK	(1 << 0)
#define DTH_SWITCH_MASK		(1 << 1)
#define BMX_ACC_MASK       	(1 << 2)
#define BMX_GYRO_MASK      	(1 << 3)
#define BMX_MAGN_MASK       	(1 << 4)
#define ANALOG_MASK       	(1 << 5)

struct command
{
	uint8_t prefix;
	uint8_t type;
	uint32_t body;
}__attribute__ ((packed));

void init_command(uint8_t type, uint32_t body, struct command* cmd);

#endif
