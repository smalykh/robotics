#ifndef __PACKET_STRTUCTS_H__
#define __PACKET_STRTUCTS_H__

typedef enum {BUTTON, DTH22, ANALOG, BMX} packet_t;

struct button_pack
{
	char is_pressed;
} __attribute__((packed));

struct analog_pack
{
	int  value;
} __attribute__((packed));


struct dth22_pack
{
	int temperature;
	int humidity;
} __attribute__((packed));

typedef enum {ACC, GYRO, MAGN} bmx_type;
static const char *bmx_type_str[] = {"ACC", "GYRO", "MAGN"} ;
struct bmx_pack
{
	bmx_type type;
	float x;
	float y;
	float z;
} __attribute__((packed));

#endif
