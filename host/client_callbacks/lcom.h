#ifndef __LCOM_H__
#define __LCOM_H__

#include <stdio.h>
#include "packet.h"

/* This file contains mock of client_app - low_level_app API fucntions
*/ 

void* make_context();
int publish (void *context, packet_t type, void* pdata, int length);

#endif
