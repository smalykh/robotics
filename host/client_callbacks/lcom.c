#include "lcom.h"
//#define PLOTMODE

void* make_context()
{
	// Some kind of magic here
	return (void*)0xc0fecafe;
}

int publish (void *context, packet_t type, void* pdata, int length)
{
	switch (type)
	{
		case BUTTON: {
			struct button_pack *pack = (struct button_pack*)pdata;
			printf("Status of button is %s\n", 
				(pack->is_pressed)? "pressed": "released");
			break;
		}
		case DTH22: {
			struct dth22_pack *pack = (struct dth22_pack*)pdata;
			printf("Temperature: %d. Humidity: %d per.\n",
					pack->temperature, pack->humidity);
			break;
		}
		case ANALOG: {
			struct analog_pack *pack = (struct analog_pack*)pdata;
			printf("Analog: %d\n", pack->value);
			break;
		}
		case BMX: {
			struct bmx_pack *p = (struct bmx_pack*)pdata;
			#ifdef PLOTMODE
			fprintf(stderr, "%02.02f;%02.02f;%02.02f\n",p->x, p->y, p->z);
				
			#else
			printf("%s:  X:%02.02f Y:%02.02f Z:%02.02f\n", 
				bmx_type_str[p->type], p->x, p->y, p->z);
			#endif
			break;
		}
	}
}
