#include "serial_port.h"
#include "stdlib.h"

// Initialise serial config by default values
void serial_port_set_defaults(struct serial_config *s)
{
	if( s != NULL)
	{
		s->baudrate 	= SERIAL_PORT_BAUDRATE_DEFAULT;
		s->bits 	= SERIAL_PORT_BITS_DEFAULT;
		s->parity	= SERIAL_PORT_PARITY_DEFAULT;
		s->stopbits	= SERIAL_PORT_STOPBITS_DEFAULT;
	}
}

static char none_parity_opt[] = "-parenb";
static char even_parity_opt[] = "-parodd";
static char odd_parity_opt [] = "parodd";

int stty_config(const char* device_filename, struct serial_config *s)
{
	printf("serial port config..\r\n");
	char command[256];
	char *parity_opt;

	if(s->parity == 'N') 		parity_opt = none_parity_opt;
	else if (s->parity == 'E') 	parity_opt = even_parity_opt;
	else if (s->parity == 'O') 	parity_opt = odd_parity_opt;
	else return -EINVAL;

	snprintf(command, sizeof(command), "stty -F %s %d cs%d %ccstopb %s",
					device_filename, s->baudrate, s->bits, 
					(s->stopbits == 1)? '-' : ' ', parity_opt);  
	printf("%s\r\n", command);
	int retval = system(command);//"sudo stty -F /dev/ttyS3 9600 cs8 -cstopb -parenb");
	return retval;
}
// Service func for serial port setting. 
int set_interface_attribs (int fd, int speed, int parity)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
//        if (tcgetattr (fd, &tty) != 0)
 //       {
  //              fprintf (stderr, "error %d from tcgetattr", errno);
   //             return -1;
    //    }

        cfsetospeed (&tty, speed);
        cfsetispeed (&tty, speed);

        tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;     // 8-bit chars
        // disable IGNBRK for mismatched speed tests; otherwise receive break
        // as \000 chars
        tty.c_iflag &= ~IGNBRK;         // disable break processing
        tty.c_lflag = 0;                // no signaling chars, no echo,
                                        // no canonical processing
        tty.c_oflag = 0;                // no remapping, no delays
        tty.c_cc[VMIN]  = 0;            // read doesn't block
        tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

        tty.c_iflag &= ~(IXON | IXOFF | IXANY); // shut off xon/xoff ctrl

        tty.c_cflag |= (CLOCAL | CREAD);// ignore modem controls,
                                        // enable reading
        tty.c_cflag &= ~(PARENB | PARODD);      // shut off parity
        tty.c_cflag |= parity;
        tty.c_cflag &= ~CSTOPB;
        tty.c_cflag &= ~CRTSCTS;

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
        {
                fprintf (stderr, "error %d from tcsetattr", errno);
                return -1;
        }
        return 0;
}

// Service func for serial port setting. 
void set_blocking (int fd, int should_block)
{
        struct termios tty;
        memset (&tty, 0, sizeof tty);
        if (tcgetattr (fd, &tty) != 0)
        {
                fprintf(stderr, "error %d from tggetattr", errno);
                return;
        }

        tty.c_cc[VMIN]  = should_block ? 1 : 0;
        tty.c_cc[VTIME] = 5;            // 0.5 seconds read timeout

        if (tcsetattr (fd, TCSANOW, &tty) != 0)
                fprintf (stderr, "error %d setting term attributes", errno);
}

// Function for translate numbers like 115200, 9600 
// to system enums that processed set_interface_attribs.
// Speed - speed of serial port.
// Returns system enum for set_interface_attribs work.
int get_speed_enum_by_int(int speed)
{
	switch(speed)
	{
		case 115200: 	return B115200;
		case 57600: 	return B57600;
		case 38400: 	return B38400;
		case 19200: 	return B19200;
		case 9600: 	return B9600;
		default:	return B9600;
	}
}

// Main function in file. Initialise serial port.
// portname	- device file of interface
// config	- configuration of serial port: speed, parity and so on
// fd		- pointer to int. File descriptor of serial port will be returned here.
// dbg		- DBG flag. Function returns mock insted of serial port descriptor.
// Rerurns 0 if succes, other else.
int init_interface(const char * portname, struct serial_config config, int *fd)
{
	int fd_tty = open (portname, O_RDWR | O_NOCTTY | O_SYNC);
	if (fd_tty < 0)
	{
      		 	fprintf(stderr, "error %d opening %s: %s", errno, portname, strerror (errno));
       	 	return -1;
	}

	// set speed to 9600 bps, 8n1 (no parity)
	// TODO: apply 8n1 and so on
	set_interface_attribs (fd_tty, get_speed_enum_by_int(config.baudrate), 0);
	set_blocking (fd_tty, 0);   // set no blocking
	
	sleep(5);
	// Return desc
	*fd = fd_tty;
	return 0;
}

