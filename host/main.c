#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "serial_port.h"
#include "command.h"
#include "client_callbacks/lcom.h"
#include <time.h>
	
//TODO: Make a parameter
#define DEVICE_PATH	"/dev/ttyUSB0"
//#define DEBUG

// Function send command to interface
// Returns 0 if success, -1 if there was transmitting error
int send_command(int fd, uint8_t *request)
{
	#ifdef DEBUG
	for(int i=0; i < sizeof(struct command); i++) 
		printf("%x ", *(request + i));
	printf("\r\n");
	#endif

	// Send command to interface
	int bytesWriten = write(fd, request, sizeof(struct command));
	if(bytesWriten != sizeof(struct command))
	{
		fprintf(stderr, "Problem with writing data to interface. Abort\r\n");
		return -1;
	}
	else return 0;
}

enum RSM_state {WAIT_HEADER, WAIT_LENGTH, WAIT_BYTES, SUCCESS};

struct state_machine
{
	enum RSM_state state;
	int bytes_expected;
	int bytes_written;
};

void SM_init(struct state_machine *sm)
{
	sm->state = WAIT_HEADER;
	sm->bytes_written  = 0;
	sm->bytes_expected = 0;
}
int 
SM_process_byte(struct state_machine *sm, uint8_t r_byte, uint8_t *buffer, int buffer_len)
{
	switch(sm->state) {
		case WAIT_HEADER:
			if(r_byte != COMMAND_PREFIX) return EINVAL;
			sm->state = WAIT_LENGTH;	
			break;
	
		case WAIT_LENGTH:
			if(r_byte >= buffer_len) return ENOMEM;
			sm->bytes_expected = r_byte;
			sm->state = WAIT_BYTES; 
			break;
	
		case WAIT_BYTES:
			buffer[sm->bytes_written++] = r_byte;
			sm->bytes_expected--;
			// If we wait another data return EAGAIN 
			// No reset needed in this case
			if(sm->bytes_expected > 0) return EAGAIN;
			else 
			{ 
				sm->state = SUCCESS; 
				return 0;
			}
	}
}
// Function wait response to command in blocking mode with specified timeout
// fd 		- file descriptor of interface
// resp 	- pointer where response will be wroten 
// Return 0 if success, -1 if there was receiving error or timeout
int receive_data(int fd, uint8_t *response, int len)
{
	int retval, r_byte;
	struct state_machine sm = {0};
	SM_init(&sm);
	
	// Read an answer from interface
	while(1)
	{	
		// Read one byte
		retval = read(fd, &r_byte, 1);

		#ifdef DEBUG
		printf("read retval = %d\r\n", retval);
		#endif

		// If error then break
		if(retval <  0) break;
		// If no data, try again
		if(retval == 0) continue;
		// Else process received byte
		if(retval >  0) 
		{
			retval = SM_process_byte(&sm, r_byte, response, len);
			if(!retval) break;

			switch(retval)
			{
				case ENOMEM: return retval; // User buf too litle
				case EAGAIN: // That's ok.				
				case EINVAL: continue; // Filter trash in interface.
			}
		}
	}
	if(retval)
	{
		errno = retval;
		return -1;
	}
	else return sm.bytes_written;
}
			
#define GET_XYZ(x,y,z, buf) do {x = (float) *((float*) &buf[1]); \
				y = (float) *((float*) &buf[5]); \
				z = (float) *((float*) &buf[9]);} while (0); 
	
static uint8_t buf[256];
int main(int argc, char **argv)
{
	int fd = 0;
	int err = 0;
	int bytes_read = 0;
	uint32_t response = 0;
	float x, y, z;
			
	// Set default serial port configuration
	struct serial_config s_conf = {0};
	serial_port_set_defaults(&s_conf);  		

	// Initialise interface for communication
	err = init_interface(DEVICE_PATH, s_conf, &fd);
	if(err)
	{
		fprintf(stderr, "Cant initialise interface for robot's communication.\n");
		fprintf(stderr, "Abort.\n");
		goto init_err;		
	}
	// Initialise template command
	struct command cmd = {0};
	init_command(0, ///* BUTTON_SWITCH_MASK */ DTH_SWITCH_MASK |
	//		ANALOG_MASK |
	//	 BMX_ACC_MASK |  BMX_GYRO_MASK | 
	BMX_MAGN_MASK, 
			&cmd);

	// Process command
	err = send_command(fd, (uint8_t*)&cmd);
	if(err) fprintf(stderr, "Error occured. Code=%d\n", errno);
	while(1)
	{
		int type;
		void *context;
		memset(buf, 0, sizeof(buf));
		bytes_read = receive_data(fd, buf, sizeof(buf));
		if(bytes_read < 0) goto receive_err;
		type = buf[0];

		//Some magic need to happen	
		context = make_context();
		
		switch (type)
		{
			case BUTTON_SENSOR_DATA: {
				struct button_pack pack = { buf[1] };
				publish(context, BUTTON, &pack, sizeof(pack)); 
				break;
			}
			case DHT22_SENSOR_DATA: {
				struct dth22_pack pack = { (int)buf[2], (int)buf[1] };
				publish(context, DTH22, &pack, sizeof(pack)); 
				break;
			}
			case BMX_ACC_SENSOR_DATA: {
				GET_XYZ(x, y, z, buf);
				struct bmx_pack pack = { ACC, x, y, z };
				publish(context, BMX, &pack, sizeof(pack)); 
				break;
			}
			case BMX_GYRO_SENSOR_DATA: {
				GET_XYZ(x, y, z, buf);
				struct bmx_pack pack = { GYRO, x, y, z };
				publish(context, BMX, &pack, sizeof(pack)); 
				break;
			}
			case BMX_MAGN_SENSOR_DATA: {
				GET_XYZ(x, y, z, buf);
				printf("%f\n", z);
				struct bmx_pack pack = { MAGN, x, y, z };
				publish(context, BMX, &pack, sizeof(pack)); 
				break;
			}
			case ANALOG_SENSOR_DATA: {
				struct analog_pack pack = { *((int*)&buf[1]) };
				publish(context, ANALOG, &pack, sizeof(pack)); 
				break;
			}
		}
		//sleep(1);
	}

receive_err:
init_err:
	// Close interface
	close(fd);
	return err;
}
