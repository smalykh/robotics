#ifndef __SERIAL_PORT__
#define __SERIAL_PORT__
#include <stdio.h>
#include <errno.h>
#include <fcntl.h> 
#include <string.h>
#include <termios.h>
#include <unistd.h>

#define SERIAL_PORT_BAUDRATE_DEFAULT	9600	
#define SERIAL_PORT_BITS_DEFAULT	8	
#define SERIAL_PORT_PARITY_DEFAULT	'N'	
#define SERIAL_PORT_STOPBITS_DEFAULT	1	

// Configuration of serial port
struct serial_config
{
	int baudrate;
	unsigned char bits;
	char parity;
	unsigned char stopbits;
};

// Fill serial config struct by default values 115200-8N1
void serial_port_set_defaults(struct serial_config *s);

// Initialise serial port. 
// portname      - name of device like /dev/ttyUSB0
// config        - baudtate and etc.
// fd            - file descriptor of open device
// dbg           - work with virtual null modem device
// Returns 0 if succes, -1 else
int init_interface(const char * portname, struct serial_config config, int *fd);

#endif
